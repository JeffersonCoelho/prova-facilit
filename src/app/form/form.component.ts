import { City } from './city';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { User } from './user';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  user: User;
  cities: City[];
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.user = new User();

    this.cities = this.addCities();
  }

  ngOnInit() {
    this.validations();
  }


  public submit() {

    if (this.form.valid) {
      alert('Cadastrado!');
    } else {
      alert('Verifique os campos!');
      this.checkForm(this.form);
    }
  }

  private reset() {
    this.form.reset();
  }

  private addCities() {

    return [
      new City('Recife'),
      new City('Camaragibe'),
      new City('Paulista'),
      new City('Olinda'),
      new City('Jaboatão dos Guararapes')
    ];
  }

  private checkForm(formGroup: FormGroup) {
    Object.keys(this.form.controls).forEach(input => {
      const ctrl = this.form.get(input);
      ctrl.markAsTouched();

      if (ctrl instanceof FormGroup) {
        this.checkForm(ctrl);
      }
    });
  }

  private checkValid(input: string) {
    return !this.form.controls[input].valid && this.form.controls[input].touched;
  }

  private checkEmail() {
    const email = this.form.get('email');
    if (email.errors) {
      return email.errors['email'] && email.touched;
    }
  }

  private apllyErrorInCSS(input: string) {
    return {
      'has-error': this.checkValid(input),
      'has-feedback': this.checkValid(input)
    }
  }

  private validations() {

    this.form = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, [Validators.email, Validators.required]],
      gender: [null, Validators.required],
      city: [null, Validators.required],
      experiences: [null, Validators.required],
      experienceUX: [null, Validators.required],
      experienceJS: [null, Validators.required],
      experienceUI: [null, Validators.required]
    })
  }
}
