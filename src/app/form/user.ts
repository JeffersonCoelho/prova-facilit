export class User {
    name: string;
    email: string;
    gender: { "female", "male", "other" };
    city: { "recife", "olinda", "camaragibe", "paulista", "jaboatao" }

    experiences: string;
    experiencesUX: boolean;
    experiencesJS: boolean;
    experiencesUI: boolean;

}